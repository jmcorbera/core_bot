package botSetup;

import wallet.Wallet;
import java.util.HashSet;
import java.util.Set;

import rules.AssetRule;

public class BotSetup {
	private Set <AssetRule> activeRules;
	private Set <AssetRule> inactiveRules;
	private double requestTimeInMinutes;
	private Wallet wallet;
	
	public BotSetup(Set<AssetRule> activeRules, double requestTimeInMinutes, double amount) {
		super();
		this.activeRules = activeRules!=null ? activeRules : new HashSet<AssetRule>();
		this.inactiveRules = new HashSet<AssetRule>();
		this.requestTimeInMinutes = requestTimeInMinutes;
		this.wallet = new Wallet();
		wallet.addAmount(amount);
	}
	
	public double getRequestTimeInMinutes() {
		return this.requestTimeInMinutes;
	}

	public Wallet getWallet() {
		return this.wallet;
	}
	
	public void changeActive(AssetRule assetRule) {
		boolean isActive = this.activeRules.contains(assetRule);
		boolean isInactive = this.inactiveRules.contains(assetRule);
		if(isActive) {
			this.activeRules.remove(assetRule);
			this.inactiveRules.add(assetRule);
		}
		else if(isInactive) {
			this.inactiveRules.remove(assetRule);
			this.activeRules.add(assetRule);
		}
	}
	
	public Set<AssetRule> getActiveRules(){
		return this.activeRules;
	}
	
	public Set<AssetRule> getInactiveRules(){
		return this.inactiveRules;
	}

}
