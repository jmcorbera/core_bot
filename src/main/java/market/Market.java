package market;

import wallet.assets.Asset;

public interface Market {
	public double getPrice(String symbol);
	public String getCode();
	public boolean placeSellOrder(double quantity,double marketPrice, Asset asset);
	public boolean placeBuyOrder(double quantity,double marketPrice, Asset asset);
}
