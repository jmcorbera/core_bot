package market;

import wallet.assets.Asset;

public class MarketProxy implements Market{
	private Market market;

	public MarketProxy(Market market) {
		this.market = market;
	}

	@Override
	public double getPrice(String symbol) {
		return market.getPrice(symbol);
	}

	@Override
	public String getCode() {
		return market.getCode();
	}

	@Override
	public boolean placeSellOrder(double quantity, double marketPrice, Asset asset) {
		return market.placeSellOrder(quantity, marketPrice, asset);
	}

	@Override
	public boolean placeBuyOrder(double quantity, double marketPrice, Asset asset) {
		return market.placeBuyOrder(quantity, marketPrice, asset);
	}

}
