package rules;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import wallet.assets.Asset;

public class AssetRule {
	private double sellCondition;
	private double buyCondition;
	private Asset asset;
	private double quantity;
	private Map<String,Double> referencePrices;
	
	public AssetRule(double sellCondition, double buyCondition, Asset asset, double quantity) {
		super();
		this.sellCondition = sellCondition;
		this.buyCondition = buyCondition;
		this.asset = asset;
		this.quantity = quantity;
		this.referencePrices = new HashMap<String,Double>();
	}

	public double getSellCondition() {
		return sellCondition;
	}

	public double getBuyCondition() {
		return buyCondition;
	}

	public Asset getAsset() {
		return new Asset(this.asset.getName(),this.asset.getSymbol());
	}

	public double getQuantity() {
		return quantity;
	}
	
	public double getReferencePrice(String marketCode) {
		if(!this.referencePrices.containsKey(marketCode))
			return -1;
		return this.referencePrices.get(marketCode);
	}

	public void addReferencePrice(String market, double referencePrice) {
		this.referencePrices.put(market, referencePrice);
	}
	
	@Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
 
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

}
