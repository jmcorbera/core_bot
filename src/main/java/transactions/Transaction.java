package transactions;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import wallet.assets.Asset;

public class Transaction {
	private double price;
	private Date currentDate;
	private double quantity;
	private Asset asset;
	private TransactionType type;

	public Transaction(double price, Date currentDate, double quantity, Asset asset, TransactionType type) {
		super();
		this.price = price;
		this.currentDate = currentDate;
		this.quantity = quantity;
		this.asset = asset;
		this.type = type;
	}

	public double getPrice() {
		return price;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public double getQuantity() {
		return quantity;
	}

	public Asset getAsset() {
		return asset;
	}

	public double getTotalPrice() {
		return this.getPrice() * this.getQuantity();
	}

	public TransactionType getType() {
		return this.type;
	}

	@Override
	public String toString() {
		return 
				"Type: " + this.getType().toString() + 
				"\nCryptocurrency \nName: " + 
				this.getAsset().getName()
				+ "\nSymbol: " + this.getAsset().getSymbol() + 
				"\nPrice: " + price + 
				"\nQuantity: " + this.quantity
				+ "\nTotal Price: " + this.getTotalPrice() + 
				"\nDate: " + currentDate.toString();
	}
	
	@Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
 
    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
}
