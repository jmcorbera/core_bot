package transactions;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import bot.Bot;

public class TransactionHistory implements PropertyChangeListener,
java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Transaction> sells;
	private List<Transaction> buys;
	
	public TransactionHistory(Bot bot) {
		super();
		this.sells = new ArrayList<Transaction>();
		this.buys =  new ArrayList<Transaction>();
		bot.addPropertyChangeListener(this);
	}
	
	public Transaction getSell(int index) {
		return index >= 0 && index < sells.size() ? sells.get(index): null;
	}
	
	public Transaction getBuy(int index) {
		return index >= 0 && index < buys.size() ? buys.get(index): null;
	}
	
	public int getBuysSize() {
		return buys.size();
	}
	
	public int getSellsSize() {
		return sells.size();
	}
	
	public void setSell(int id,Transaction assetTransaction) {
		if(this.sells.size()-1>=id)
			this.sells.set(id, assetTransaction);
	}
	
	public void setBuy(int id,Transaction assetTransaction) {
		if(this.buys.size()-1>=id)
			this.buys.set(id, assetTransaction);
	}
	
	public void add(Transaction assetTransaction) {
		if(assetTransaction.getType() == TransactionType.BUY)
			this.buys.add(assetTransaction);
		if(assetTransaction.getType() == TransactionType.SELL)
			this.sells.add(assetTransaction);
	}

	@Override
	public void propertyChange(PropertyChangeEvent change) {
		add((Transaction) change.getNewValue());
	}
}
