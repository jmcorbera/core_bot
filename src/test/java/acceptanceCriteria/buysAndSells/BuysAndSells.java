package acceptanceCriteria.buysAndSells;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import bot.Bot;

public class BuysAndSells {
	private double rulePrice;
	private double referencePrice;
	private double marketPrice;
	

	@Before
	public void init() {
		MockitoAnnotations.openMocks(this);
		this.rulePrice = 5;
		this.referencePrice = 100;
		this.marketPrice = 100;
	}

	@Test
	public void testCheckBuyOkDecreasedMoreThanTheRule() {
		assertTrue(Bot.mustBuy(increasePrice(this.rulePrice + 1, this.referencePrice), this.rulePrice, this.marketPrice));
	}

	@Test
	public void testCheckBuyOkDecreasedTheSameThanTheRule() {
		assertTrue(Bot.mustBuy(increasePrice(this.rulePrice, this.referencePrice), this.rulePrice, this.marketPrice));
	}

	@Test
	public void testCheckBuyFailMarketPriceIncreased() {
		assertFalse(Bot.mustBuy(this.referencePrice, this.rulePrice, increasePrice(this.rulePrice, this.marketPrice)));
	}

	@Test
	public void testCheckBuyFailPriceNotChanged() {
		assertFalse(Bot.mustBuy(this.referencePrice, this.rulePrice, this.marketPrice));
	}

	@Test
	public void testCheckSellFailPriceNotChanged() {
		assertFalse(Bot.mustSell(this.referencePrice, this.rulePrice, this.marketPrice));
	}

	@Test
	public void testCheckSellFailMarketPriceDecreased() {
		assertFalse(Bot.mustSell(this.referencePrice, this.rulePrice, decreasePrice(this.rulePrice, this.marketPrice)));
	}

	@Test
	public void testCheckSellOkIncreasedTheSameThanTheRule() {
		assertTrue(Bot.mustSell(this.referencePrice, this.rulePrice, increasePrice(this.rulePrice, this.marketPrice)));
	}

	@Test
	public void testCheckSellOkIncreasedMoreThanTheRule() {
		assertTrue(Bot.mustSell(this.referencePrice, this.rulePrice,
				increasePrice(this.rulePrice + 1, this.referencePrice)));
	}
	
	private double increasePrice(double increment, double price) {
		return price + (price * increment) / 100;
	}

	private double decreasePrice(double decrement, double price) {
		return price - (price * decrement) / 100;
	}
	
}
