package mock;

import market.Market;
import wallet.assets.Asset;

public class MarketFake implements Market{

	@Override
	public double getPrice(String symbol) {
		return 100;
	}

	@Override
	public String getCode() {
		return "BINANCE";
	}

	@Override
	public boolean placeSellOrder(double quantity, double marketPrice, Asset asset) {
		return true;
	}

	@Override
	public boolean placeBuyOrder(double quantity, double marketPrice, Asset asset) {
		return true;
	}

}
