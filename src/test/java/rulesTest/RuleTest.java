/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package rulesTest;

import rules.AssetRule;
import wallet.assets.Asset;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class RuleTest {
	private AssetRule rule;
	private double quantity;
	private double percentage;
	private Asset asset;

	@Before
	public void init() {
		quantity = 1.0;
		percentage = 5.0;
		asset = new Asset("BITCOIN","BTC");
		rule = new AssetRule(percentage,percentage, asset ,quantity);
	}

	@Test
	public void testGetQuantityOk() {
		assertTrue(rule.getQuantity() == quantity);
	}

	@Test
	public void testGetBuyConditionOk() {
		assertTrue(rule.getBuyCondition() == percentage);
	}

	@Test
	public void testGetSellConditionOk() {
		assertTrue(rule.getSellCondition() == percentage);
	}
	
	@Test
	public void testGetAssetOk() {
		Asset asset_ = rule.getAsset();
		assertTrue(asset_.getName() == asset.getName() && 
				asset_.getSymbol() == asset.getSymbol());
	}
	
	@Test
	public void testGetReferencesPricesMarketNotExists() {
		assertTrue(rule.getReferencePrice("BINANCE")==-1.0);
	}
	
	@Test
	public void testGetReferencesPricesMarketExistsOk() {
		rule.addReferencePrice("BINANCE", 100.0);
		assertTrue(rule.getReferencePrice("BINANCE")==100.0);
	}
	
	@Test
	public void testSetReferencesPricesMarketExistsOk() {
		rule.addReferencePrice("BINANCE", 100.0);
		rule.addReferencePrice("BINANCE",200.0);
		assertTrue(rule.getReferencePrice("BINANCE")==200.0);
	}
	
	@Test
	public void testAssetRuleEqualsOk() {	
		AssetRule newRule = new AssetRule(percentage,percentage, asset ,quantity);
		assertTrue(rule.equals(newRule));
	}
	
	@Test
	public void testAssetRuleEqualsFail() {	
		AssetRule newRule = new AssetRule(0,percentage, asset ,quantity);
		assertFalse(rule.equals(newRule));
	}

}
